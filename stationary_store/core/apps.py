from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'stationary_store.core'