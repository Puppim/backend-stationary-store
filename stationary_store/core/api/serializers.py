from rest_framework import serializers

from stationary_store.core.models import (Commission, CommissionDay, Customers,
                                          Product, ProductList, Sale, Seller)


class ProductListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductList
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

    def validate_max_commission(self, value):
        max_commission = value
        if max_commission > 10 or max_commission < 0:
            raise serializers.ValidationError(
                'Must be between 0 to 10% commission')
        return max_commission


class CommissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Commission
        fields = '__all__'


class CommissionDaySerializer(serializers.ModelSerializer):
    class Meta:
        model = CommissionDay
        fields = '__all__'




class SellerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seller
        fields = ('id','first_name', 'last_name', 'email', 'telephone')


class CustomersSerializer(serializers.ModelSerializer):

    class Meta:
        model = Customers
        fields = ('id','first_name', 'last_name', 'email', 'telephone')

    # def validate(self, attrs):
    #     print(attrs)
    #     if len(attrs) < 2:
    #         raise serializers.ValidationError('Need email and telephone')
    #     return super().validate(attrs)

    # def validate_telephone(self, value):
    #     telephone = value
    #     if len(telephone) < 5:
    #         raise serializers.ValidationError('Must have at least 5 chars')
    #     print(value)
    #     return telephone

    # def validate_email(self, value):
    #     email = value
    #     if len(email) < 5:
    #         raise serializers.ValidationError('Must have at least 5 chars')
    #     print(value)
    #     return email

class SaleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sale
        fields = ('id','number_nfe','customers', 'customers', 'commission_day', 'seller', 'total_amount', 'created_at', 'updated_at')

class SaleDepthSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sale
        depth = 1
        fields = ('id','number_nfe','customers', 'customers', 'commission_day', 'seller', 'total_amount', 'created_at', 'updated_at')
