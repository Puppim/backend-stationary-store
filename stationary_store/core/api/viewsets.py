from datetime import datetime
from decimal import Decimal

from django_filters import rest_framework as filters
from rest_framework import status, viewsets
from rest_framework.filters import OrderingFilter, SearchFilter
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from stationary_store.api_version import API_Version
from stationary_store.core.api.serializers import (CommissionDaySerializer,
                                                   CommissionSerializer,
                                                   CustomersSerializer,
                                                   ProductListSerializer,
                                                   ProductSerializer,
                                                   SaleSerializer,
                                                   SaleDepthSerializer,
                                                   SellerSerializer)
from stationary_store.core.models import (Commission, CommissionDay, Customers,
                                          Product, ProductList, Sale, Seller)


class ApiVersion(viewsets.ViewSet):

    def list(self, request):
        return Response({'API_Version': API_Version})


class ProductListViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = ProductList.objects.all()
    serializer_class = ProductListSerializer


    def create(self, request):

        serializer = ProductListSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        productlist = ProductList.objects.create(**serializer.validated_data)
        productlist.created_at = datetime.now()
        productlist.save()
        commissiobase = productlist.product.commission
        try:
            comissio_day = CommissionDay.objects.filter(
                days=productlist.created_at.weekday()).latest('created_at')
            if (commissiobase > comissio_day.max_commission):
                commissiobase = comissio_day.max_commission
            elif (commissiobase < comissio_day.min_commission):
                commissiobase = comissio_day.min_commission
        except:
            commissiobase = productlist.product.commission
        
        percernt_commision = 0.01*float(commissiobase)
        commission_amount = Decimal(float(
            productlist.quantity)*float(productlist.product.unitary_value)*percernt_commision
        )
        Commission.objects.create(
            seller=productlist.sale.seller,
            productList=productlist,
            commission_amount=commission_amount
        )
        sale = Sale.objects.get(id=productlist.sale.id)
        sale.total_amount=commission_amount+sale.total_amount
        sale.save()
        # return productlist
        return Response(serializer.data)


class CommissionViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Commission.objects.all()
    serializer_class = CommissionSerializer
    filter_backends = (filters.DjangoFilterBackend,
                       SearchFilter, OrderingFilter)
    filter_fields = ('created_at', 'id')
    ordering_fields = ('created_at',)
    search_fields = ('created_at', 'seller')

    def list(self, request):
        seller_params = self.request.query_params.get('seller')
        queryset = Commission.objects.filter(seller__id=seller_params)
        serializer = CommissionSerializer(queryset, many=True)
        return Response({'results': serializer.data})


class CommissionDayViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = CommissionDay.objects.all()
    serializer_class = CommissionDaySerializer


class ProductViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    filter_backends = (filters.DjangoFilterBackend,
                       SearchFilter, OrderingFilter)
    filter_fields = ('created_at', 'id')
    ordering_fields = ('created_at',)
    search_fields = ('description',)


class SaleViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer

class SaleDepthViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Sale.objects.all()
    serializer_class = SaleDepthSerializer


class SellerViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Seller.objects.all()
    serializer_class = SellerSerializer


class CustomersViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Customers.objects.all()
    serializer_class = CustomersSerializer

