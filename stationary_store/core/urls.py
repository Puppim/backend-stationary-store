from django.contrib import admin
from django.urls import path
from rest_framework.routers import DefaultRouter

from stationary_store.core.api.viewsets import (ApiVersion,
                                                CommissionDayViewSet,
                                                CommissionViewSet,
                                                CustomersViewSet,
                                                SaleDepthViewSet,
                                                ProductListViewSet,
                                                ProductViewSet, SaleViewSet,
                                                SellerViewSet)

app_name = 'core'


router = DefaultRouter()
router.register('version', ApiVersion, basename='version')
router.register('productlist', ProductListViewSet, basename='product_list')
router.register('commission', CommissionViewSet, basename='commission')
router.register('commissionday', CommissionDayViewSet,
                basename='commissionday')
router.register('product', ProductViewSet, basename='product')
router.register('sale', SaleViewSet, basename='sale')
router.register('saleall', SaleDepthViewSet, basename='saleall')
router.register('customers', CustomersViewSet, basename='customers')
router.register('seller', SellerViewSet, basename='seller')

urlpatterns = [
    path('admin/', admin.site.urls),
]
