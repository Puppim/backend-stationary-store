from django.contrib.auth.models import User
from rest_framework.test import APIRequestFactory
from rest_framework.test import APITestCase
from rest_framework.test import force_authenticate

from ..api.viewsets import CustomersViewSet, SellerViewSet
from stationary_store.core.models import Customers, Seller
from ..util.api_test_helpers import get_token


class APICustomerssViewSetsTest(APITestCase):
    fixtures = ['user.json']

    def setUp(self):
        self.request = APIRequestFactory().get("")
        user = User.objects.get(id=1)
        force_authenticate(self.request, user=user, token=get_token(self.client))

    def test_employee_view_set(self):
        customer_detail = CustomersViewSet.as_view({'get': 'retrieve'})

        ducktales = Customers.objects.create(
            first_name="Tio",
            last_name="Patinha",
            email="tiopatinhas@teste.com",
            telephone="119999999",
        )

        response = customer_detail(self.request, pk=ducktales.pk)

        self.assertEqual(response.status_code, 200)

    def test_department_view_set(self):
        seller_detail = SellerViewSet.as_view({'get': 'retrieve'})

        ducktales = Seller.objects.create(
            first_name="Tio",
            last_name="Patinha",
            email="tiopatinhas@teste.com",
            telephone="119999999",
        )

        response = seller_detail(self.request, pk=ducktales.pk)

        self.assertEqual(response.status_code, 200)