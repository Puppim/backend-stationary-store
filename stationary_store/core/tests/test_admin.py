from django.contrib import admin
from django.test import TestCase

from stationary_store.core.admin import (CustomersModelAdmin, SaleModelAdmin, SellerModelAdmin,
                                        CommissionDayModelAdmin, ProductModelAdmin, ProductListModelAdmin
                                        )
from stationary_store.core.models import Customers, Sale, Seller, CommissionDay, Product, ProductList


class CustomersAdminTest(TestCase):

    def setUp(self):
        self.model_admin = CustomersModelAdmin(Customers, admin.site)

    def test_customers_admin_is_registered(self):
        self.assertTrue(admin.site._registry[Customers])

    def test_list_display_fields(self):
        fields_expecteds = [
            "first_name",
            "last_name",
            "email",
            "telephone",
            "id",
        ]
        for field in fields_expecteds:
            with self.subTest():
                self.assertIn(field, self.model_admin.list_display)


class SaleAdminTest(TestCase):

    def setUp(self):
        self.model_admin = SaleModelAdmin(Sale, admin.site)

    def test_sale_admin_is_registered(self):
        # pylint: disable=W0212
        self.assertTrue(admin.site._registry[Sale])

    def test_list_display_fields(self):
        fields_expecteds = [
           "id",
            "number_nfe",
            "customers",
            "seller",
            "created_at",
        ]

        for field in fields_expecteds:
            with self.subTest():
                self.assertIn(field, self.model_admin.list_display)


class SellerAdminTest(TestCase):

    def setUp(self):
        self.model_admin = SellerModelAdmin(Seller, admin.site)

    def test_sale_admin_is_registered(self):
        # pylint: disable=W0212
        self.assertTrue(admin.site._registry[Seller])

    def test_list_display_fields(self):
        fields_expecteds = [
            "id",
            "first_name",
            "last_name",
            "email",
            "telephone",
        ]

        for field in fields_expecteds:
            with self.subTest():
                self.assertIn(field, self.model_admin.list_display)

class CommissionDayAdminTest(TestCase):

    def setUp(self):
        self.model_admin = CommissionDayModelAdmin(CommissionDay, admin.site)

    def test_customers_admin_is_registered(self):
        self.assertTrue(admin.site._registry[CommissionDay])

    def test_list_display_fields(self):
        fields_expecteds = [
            "id",
            "days",
            "created_at",
        ]
        for field in fields_expecteds:
            with self.subTest():
                self.assertIn(field, self.model_admin.list_display)

class ProductModelAdminAdminTest(TestCase):

    def setUp(self):
        self.model_admin = ProductModelAdmin(Product, admin.site)

    def test_customers_admin_is_registered(self):
        self.assertTrue(admin.site._registry[Product])

    def test_list_display_fields(self):
        fields_expecteds = [
          "id",
        "description",
        "unitary_value",
        "commission",
        "created_at",
        ]
        for field in fields_expecteds:
            with self.subTest():
                self.assertIn(field, self.model_admin.list_display)


class ProductListAdminTest(TestCase):

    def setUp(self):
        self.model_admin = ProductListModelAdmin(ProductList, admin.site)

    def test_sale_admin_is_registered(self):
        # pylint: disable=W0212
        self.assertTrue(admin.site._registry[ProductList])

    def test_list_display_fields(self):
        fields_expecteds = [
            "id",
            "product",
            "sale",
            "quantity",
            "created_at",
        ]

        for field in fields_expecteds:
            with self.subTest():
                self.assertIn(field, self.model_admin.list_display)