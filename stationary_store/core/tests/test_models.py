from django.contrib.auth.models import User
from django.test import TestCase
from model_mommy import mommy

from stationary_store.core.models import Customers, Seller


class CustomersModel_test(TestCase):
    def setUp(self):

        self.new_customer = {
            "first_name": "Tio",
            "last_name": "Patinhas",
            "email": "tiopatinhas@teste.com",
            "telephone": "99999999",
        }

        self.customer = Customers(**self.new_customer)
        self.customer.save()

    def test_new_customer_was_saved(self):
        self.assertTrue(Customers.objects.exists())

    def test_str_return_customer_name(self):
        self.assertEqual(self.new_customer["email"], str(self.customer.email))


