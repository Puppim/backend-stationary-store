from rest_framework import status
from rest_framework.test import APITestCase


class APIUrlTest(APITestCase):

    def test_customers_url_without_authentication(self):
        '''
        list all customers saved
        '''
        ...
        response = self.client.get('/api/customers/')
        print(response.status_code)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_seller_url_without_authentication(self):
        '''
        list all seller
        '''
        response = self.client.get('/api/seller/')
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_sale_url_without_authentication(self):
        '''
        list all sale
        '''
        response = self.client.get('/api/sale/')
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_products_url_without_authentication(self):
        '''
        list all product
        '''
        response = self.client.get('/api/product/')
        print(response.status_code)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
  
    def test_commissioday_url_without_authentication(self):
        '''
        list all product
        '''
        response = self.client.get('/api/commissionday/')
        print(response.status_code)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_commissio_url_without_authentication(self):
        '''
        list all product
        '''
        response = self.client.get('/api/commission/?seller=1')
        print(response.status_code)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED