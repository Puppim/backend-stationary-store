from rest_framework.test import APITestCase

from stationary_store.core.api.serializers import CustomersSerializer, CommissionDaySerializer
from stationary_store.core.models  import Customers, CommissionDay


class APICustomersSerializersTest(APITestCase):

    def test_customers_serializer(self):
        ducktales = Customers.objects.create(
            first_name="Tio",
            last_name="Patinhas",
            email="tiopatinhas@teste.com",
            telephone="9999999"
        )

        customers_serializer = CustomersSerializer(ducktales)

        self.assertIsNotNone(customers_serializer.data)
        self.assertEqual("Tio", customers_serializer.data['first_name'])
        self.assertEqual("Patinhas", customers_serializer.data['last_name'])
        self.assertEqual("tiopatinhas@teste.com", customers_serializer.data['email'])
        self.assertEqual("9999999", customers_serializer.data['telephone'])

    def test_weekday_serializer(self):
        weekday = CommissionDay.objects.create(
                days=1,
                max_commission=4,
                min_commission=2
                )
        weekday_serializer = CommissionDaySerializer(weekday)

        self.assertIsNotNone(weekday_serializer.data)
        self.assertEqual(weekday.days, weekday_serializer.data['days'])
        self.assertEqual(weekday.max_commission, weekday_serializer.data['max_commission'])
