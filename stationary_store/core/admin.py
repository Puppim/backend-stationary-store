from django.contrib import admin

from .models import (Commission, CommissionDay, Customers, Product,
                     ProductList, Sale, Seller)


@admin.register(Commission)
class CommissionModelAdmin(admin.ModelAdmin):
    list_display = [

        "id",
        "seller",
        "productList",
        "commission_amount",
        "created_at",
    ]
    # list_display_links = ["seller", ]
    # search_fields = ["seller", ]
    # readonly_fields = ['id', ]


@admin.register(ProductList)
class ProductListModelAdmin(admin.ModelAdmin):
    list_display = [

        "id",
        "product",
        "sale",
        "quantity",
        "created_at",
    ]
    list_display_links = ["created_at", ]
    search_fields = ["sale", ]
    readonly_fields = ['id', ]


@admin.register(Product)
class ProductModelAdmin(admin.ModelAdmin):
    list_display = [

        "id",
        "description",
        "unitary_value",
        "commission",
        "created_at",
    ]
    list_display_links = ["description", ]
    search_fields = ["description", ]
    readonly_fields = ['id', ]


@admin.register(Sale)
class SaleModelAdmin(admin.ModelAdmin):
    list_display = [

        "id",
        "number_nfe",
        "customers",
        "seller",
        "created_at",
    ]
    list_display_links = ["number_nfe", ]
    search_fields = ["number_nfe", ]
    readonly_fields = ['id', ]


@admin.register(CommissionDay)
class CommissionDayModelAdmin(admin.ModelAdmin):
    list_display = [

        "id",
        "days",
        "created_at",
    ]
    list_display_links = ["days", ]
    search_fields = ["days", ]
    readonly_fields = ['id', ]


@admin.register(Seller)
class SellerModelAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "first_name",
        "last_name",
        "email",
        "telephone",

    ]
    list_display_links = ["first_name", "telephone", "email"]
    search_fields = ["telephone", "email"]
    readonly_fields = ['id']


@admin.register(Customers)
class CustomersModelAdmin(admin.ModelAdmin):
    list_display = [
        "first_name",
        "last_name",
        "email",
        "telephone",
        "id",
    ]
    list_display_links = ["first_name", "telephone", "email"]
    search_fields = ["telephone", "email"]
    readonly_fields = ['id']
