# from django.contrib.auth.models import User
from django.db import models


# Clientes e vendedores têm nome, e-mail e telefone.
class Customers(models.Model):
    first_name = models.CharField(
        max_length=80, verbose_name="Nome", blank=False, null=False)
    last_name = models.CharField(
        max_length=80, verbose_name="Sobrenome", blank=False, null=False)
    email = models.CharField(
        max_length=70, verbose_name="E-mail", blank=True, null=True)
    telephone = models.CharField(
        max_length=70, verbose_name="Telefone", blank=False, null=False)

    def __str__(self):
        return self.email

    def get_fullname(self):
        return self.user.first_name+" "+self.user.last_name


class Seller(models.Model):
    first_name = models.CharField(
        max_length=80, verbose_name="Nome", blank=False, null=False)
    last_name = models.CharField(
        max_length=80, verbose_name="Sobrenome", blank=False, null=False)
    email = models.CharField(
        max_length=70, verbose_name="E-mail", blank=True, null=True)
    telephone = models.CharField(
        max_length=70, verbose_name="Telefone", blank=False, null=False)

    def __str__(self):
        return self.email

    def get_fullname(self):
        return self.user.first_name+" "+self.user.last_name


# Comissão do dia
class CommissionDay(models.Model):
    DAYS_OF_WEEK = (
        (0, 'Monday'),
        (1, 'Tuesday'),
        (2, 'Wednesday'),
        (3, 'Thursday'),
        (4, 'Friday'),
        (5, 'Saturday'),
        (6, 'Sunday'),
    )
    days = models.SmallIntegerField(choices=DAYS_OF_WEEK)
    max_commission = models.IntegerField(verbose_name="Comissão Máxima")
    min_commission = models.IntegerField(verbose_name="Comissão miníma")
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return str(self.days)

    def get_weekday(self):
        day = self.DAYS_OF_WEEK[self.days][1]
        return day


# Produto: Código |Descrição | Valor unitário
# | Percentual de Comissão (0 a 10%)


class Product(models.Model):
    description = models.CharField(
        verbose_name="Descrição", max_length=50, blank=False, null=False)
    unitary_value = models.DecimalField(
        verbose_name="Valor unitário ", max_digits=6, decimal_places=2)
    commission = models.IntegerField(
        verbose_name="Percentual de Comissão ")
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.description


# Venda: Número da nota fiscal | data/hora
# | Cliente | Vendedor | Lista de produtos com suas quantidades


class Sale(models.Model):

    number_nfe = models.CharField(
        verbose_name="Número da nota fiscal", max_length=70, blank=False, null=False)
    customers = models.ForeignKey(
        Customers, verbose_name="Cliente", blank=False, null=False, on_delete=models.PROTECT)
    commission_day = models.ForeignKey(
        CommissionDay, verbose_name="Comissão do dia", blank=False, null=False, on_delete=models.PROTECT)
    seller = models.ForeignKey(
        Seller, verbose_name="Vendedor", blank=False, null=False, on_delete=models.PROTECT)
    total_amount = models.DecimalField(
        verbose_name="Valor da comissão", default=0.00, max_digits=6, decimal_places=2, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return str(self.number_nfe)


# Lista de produtos com suas quantidades


class ProductList(models.Model):
    product = models.ForeignKey(
        Product, verbose_name="Produto", blank=False, null=False, on_delete=models.PROTECT)
    sale = models.ForeignKey(Sale, verbose_name="Venda",
                             blank=False, null=False, on_delete=models.PROTECT)
    quantity = models.IntegerField(verbose_name="Quantidade")
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return str(self.product)

# Comissão (comissão é feito aplicando-se o percentual cadastrado
#  no produto ao valor total da venda do produto (qtd * valor unitário).)


class Commission(models.Model):
    seller = models.ForeignKey(
        Seller, verbose_name="Vendedor", blank=False, null=False, on_delete=models.PROTECT)
    productList = models.ForeignKey(
        ProductList, verbose_name="Lista de Produto", blank=False, null=False, on_delete=models.PROTECT)
    commission_amount = models.DecimalField(
        verbose_name="Valor da comissão", max_digits=6, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.seller+" "+str(self.commission_amount)
